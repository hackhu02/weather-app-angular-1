// SERVICES
weatherApp.service('cityService', function () {
    this.city = 'London'
})

weatherApp.service('weatherService', ['$resource', function($resource) {
    this.GetWeather = function(city, days) {
        var weatherAPI = $resource("http://api.openweathermap.org/data/2.5/forecast?APPID=00025eef43476fa4ef530036c4bf3e3a",
        { get: { method: 'JSONP'}
        })
        return weatherAPI.get({ q: city, cnt: days })
    }
}])