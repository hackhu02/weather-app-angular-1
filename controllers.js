// CONTROLLER
weatherApp.controller('homeController', ['$scope', 'cityService', '$location', function ($scope, cityService, $location) {
    
    $scope.city = cityService.city
    $scope.$watch('city', function() {
        cityService.city = $scope.city;
    })

    $scope.submit = () => {
        $location.path('/forecast')
    }

}])

weatherApp.controller('forecastController', ['$scope', 'cityService', '$routeParams', 'weatherService', 
    function ($scope, cityService, $routeParams, weatherService) {

    $scope.days = $routeParams.days || '2'
    $scope.city = cityService.city

    $scope.weatherResult = weatherService.GetWeather($scope.city, $scope.days)

    $scope.convertToFarenheit = (degK) => Math.round((1.8 * (degK - 273)) + 32)
    $scope.convertToDate = (dt) => new Date(dt * 1000)

}])